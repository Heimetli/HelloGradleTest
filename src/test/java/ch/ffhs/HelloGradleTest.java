package ch.ffhs ;

import static org.junit.Assert.* ;
import java.io.ByteArrayOutputStream ;
import java.io.PrintStream ;
import org.junit.* ;

public class HelloGradleTest
{
   private final ByteArrayOutputStream output = new ByteArrayOutputStream() ;

   @Before
   public void setup()
   {
      System.setOut( new PrintStream(output) ) ;
   }

   @Test
   public void testMain()
   {
      HelloGradle.main( new String[] {} ) ;

      assertEquals( "Hello Gradle!", output.toString().trim() ) ;
   }

   @After
   public void cleanup()
   {
      System.setOut( null ) ;
   }
}
